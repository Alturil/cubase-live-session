# Cubase Live Session

## Hardware Setup

### Keyboard:
- Connect keyboard via MIDI, use channel out 
- MIDI > Channel > Wheel or arrows to change channel

### NanoKontrol2:

Mostly based on [this](http://blog.greggant.com/posts/2013/08/23/how-to-set-up-a-korg-nanokontrol-2-with-cubase.html).

- Via USB
- Studio > Studio Setup > + > Mackie control
  - Mackie Control options
    - MIDI Input: `nanoKONTROL2 1 SLIDER/KNOB`
    - MIDI Output: `nanoKONTROL2 1 CTRL`
  - Track quick control options
    - MIDI Input: `nanoKONTROL2 1 SLIDER/KNOB`
    - MIDI Output: `nanoKONTROL2 1 CTRL`
  - MIDI Port Setup
    - nanoKONTROL2 1 SLIDER/KNOB > In 'All MIDI Input' = `false`

### Android tablet

Maybe? Probably a bit buggy?

### Output

- Headphones (stereo)
- Balanced (check)

## Instrument setup

Specific instrument configuration.

### Halion Sonice SE

- Use multisonic (i.e: multiple instruments using different MIDI channels).
- `Inspector` > `HALion Sonic SE` > `Any` (it's MIDI 1 by default)
- Use different tracks to add extra layers, enable monitoring (or record) and match the tracks to use the same midi input

# TODO:

- Split keyboard?
    - [Filtering out notes in a range](https://steinberg.help/cubase_pro_artist/v9.5/en/cubase_nuendo/topics/midi_realtime_parameters_and_effects/midi_realtime_parameters_midi_modifiers_section_r.html)
    - Using input transformer (check browsing history)
- Session with drums on the last MIDI track, the rest keyboard, and audio for the bass (and guitar? voice?)